import datetime
from django.db import models
from django.conf import settings

# The current time and day
now = datetime.datetime.now()
today = datetime.date.today()



# A class to enable the entry of news articles into the database and to the webpage.
class NewsEntry( models.Model ):
    # without this, the plural form will be 'news entrys'
    class Meta(object):
		verbose_name_plural = 'news entries'
    # The exact time the entry is published. This will not be changed manually
    pub_date = models.DateTimeField( default = now )
    # This is the slug. Ask Sebastian what it does
    slug = models.SlugField( unique_for_date = 'pub_date', help_text = 'Only put letters and underscores here, this will be displayed in the url.')
    # The title of the entry    
    title = models.CharField(max_length=200)
    # Where the rest of the text goes.
    body = models.TextField(help_text='Use <a href="http://sphinx.pocoo.org/rest.html" target="_blank">reStructuredText</a> for formatting.')
    # The name of the author    
    author = models.CharField(max_length=100)
    # If the news entry is going to be shown on the home page or not.
    headnews = models.BooleanField('Is Head News', default=True, help_text="Whether this news will show up on the front page.")
    # When the news entry wont be on the fronpage anymore.
    obsolete_date = models.DateTimeField('Obsolescence Date', null=True, blank=True, help_text="Date after which this news will not show up on the front page.")
    
    def __unicode__(self):
        return self.title
