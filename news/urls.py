from django.conf.urls.defaults import *
from models import NewsEntry # relative import, references the models.py in this directory


info_dict = {
    'queryset': NewsEntry.objects.order_by('-pub_date'),
    'date_field': 'pub_date',
#	'template_loader': loader,
}

"""
This calls the views in news.views
"""
urlpatterns = patterns('news.views',
    url(r'^$', 'all_news'),
    url(r'^get/(?P<news_id>\d+)/$', 'news_article'), # News id is passed to news_article so that that specific entry can be shown.
)
