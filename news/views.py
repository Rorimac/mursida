# -*- coding: UTF-8 -*-

from django.conf import settings
from django.template import TemplateDoesNotExist, RequestContext

from django.shortcuts import render_to_response

from django.http import Http404
from news.models import NewsEntry

"""
Django takes all NewsEntry objects and passes them to the html files as a 'news' object
"""
def all_news(request):
    return render_to_response( 'news.html', {'news': NewsEntry.objects.all() })

"""
Django searches among the NewsEntries for an entry with the specified id and passes it to the html file
"""
def news_article( request, news_id = 1 ):
    return render_to_response( 'news_article.html', {'news_article': NewsEntry.objects.get(id = news_id) })
