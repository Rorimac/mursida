# -*- coding: UTF-8 -*-
from django.contrib.admin import TabularInline, ModelAdmin, site
from models import NewsEntry

from preview.widget import Media

# This is used so that the model NewsEntry can be accessed from the admin pages.
class EntryAdmin(ModelAdmin):
	# to get a rich editor on textarea fields:
	Media = Media
	
	list_display = ('title', 'pub_date', 'author')
#	db_table = 'blog_entries'
	ordering = ('-pub_date',)
	get_latest_by = 'pub_date'

site.register(NewsEntry, EntryAdmin)
