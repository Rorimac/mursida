# -*- coding: UTF-8 -*-
from __future__ import division

from django.forms import Textarea

class Media:
	js = (
		'js/jquery.js',
		'js/markitup/jquery.markitup.js',
		'js/markitup/sets/restructuredtext/set.js',
		'js/markItUp_init.js',
	  )
	css = {
			'screen': (
				 'js/markitup/skins/simple/style.css',
				 'js/markitup/sets/restructuredtext/style.css',
			)
	  }

class MarkItUpWidget(Textarea):
	Media = Media