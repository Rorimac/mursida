# -*- coding: UTF-8 -*-
from __future__ import division

from django.http import HttpResponse

def restructuredtext(request):
	processed = ''
	if request.method == 'POST':
		from django.contrib.markup.templatetags.markup import restructuredtext
		processed = restructuredtext(request.POST.get('data'))
	return HttpResponse(processed)