# -*- coding: UTF-8 -*-

from django.conf import settings
from django.template import TemplateDoesNotExist, RequestContext

from django.shortcuts import render_to_response

from django.http import Http404

"""
Index is unused, this is instead done through the app 'news'
"""
def index( request, name= 'index' ):
    """
    Ordinary page request.
    """
    return render_to_response('main/index.html', RequestContext(request))
	

def information( request, name = 'information' ):
    return render_to_response('main/information.html', RequestContext(request))

def kontakt(request):
    return render_to_response('main/kontakt.html', RequestContext(request))
