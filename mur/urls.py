from django.conf.urls import patterns, include, url
from mur import views


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url( r'^$', views.information, name = 'information' ),
    url( r'^kontakt/', views.kontakt, name = 'kontakt' ),
    url( r'^information/', views.information, name = 'information' ),
    ( r'^news/', include( 'news.urls' ) ),
    # Examples:
    # url(r'^$', 'mur.views.home', name='home'),
    # url(r'^mur/', include('mur.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
